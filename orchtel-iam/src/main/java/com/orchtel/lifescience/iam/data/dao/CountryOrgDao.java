package com.orchtel.lifescience.iam.data.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.orchtel.lifescience.core.response.MasterDataTypeDTO;
import com.orchtel.lifescience.iam.data.model.CountryOrg;
import com.orchtel.lifescience.iam.data.model.MasterData;

@Repository
public interface CountryOrgDao extends JpaRepository<CountryOrg, Integer> {

	List<CountryOrg> findByType(MasterDataTypeDTO type);
	Optional<CountryOrg> findByCode(String codes);	
	List<CountryOrg> findByEsIndexNameNotNullAndStatus(boolean status);
	List<CountryOrg> findByTypeAndStatusOrderByDescriptionAsc(MasterDataTypeDTO type, boolean status);
	List<CountryOrg> findByTypeOrderByDescriptionAsc(MasterDataTypeDTO type);
	CountryOrg findByTypeAndCode(MasterDataTypeDTO type, String code);
	CountryOrg findByDescriptionOrCode(String desc, String code);
	CountryOrg findByTypeAndDescriptionIgnoreCase(MasterDataTypeDTO type, String description);
	List<CountryOrg> findByRegion(MasterData region);
}