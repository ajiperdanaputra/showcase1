package com.orchtel.lifescience.iam.controller;

import java.io.Serializable;
import java.util.Optional;

import javax.validation.Valid;

import com.orchtel.lifescience.iam.data.dto.UserDTO;
import com.orchtel.lifescience.iam.service.ISettingsService;
import com.orchtel.lifescience.iam.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.orchtel.lifescience.core.response.Response;
import com.orchtel.lifescience.core.response.ResponseCode;
import com.orchtel.lifescience.core.response.ServiceException;
import com.orchtel.lifescience.iam.data.model.Company;
import com.orchtel.lifescience.iam.service.ICompanyService;

@RestController
@RequestMapping("/company")
public class CompanyController extends BaseController {

    @Autowired
    private ICompanyService compService;

    @Autowired
    private IUserService userService;

    @Autowired
    private ISettingsService settingsService;

    @PostMapping("/signup")
    public ResponseEntity<Response> registerCompany(@RequestBody @Valid final Company company) throws ServiceException {
        compService.addCompany(company);
        return success(ResponseCode.COMPANY_CREATED, company.getCmpName());
    }

    @PutMapping("/profile")
    public ResponseEntity<Response> updateCompanyProfile(@RequestBody @Valid final Company company) throws ServiceException {
        compService.updateCompanyProfile(company);
        return success(ResponseCode.COMPANY_PROFILE_UPDATED, company.getCmpName());
    }

    @GetMapping
    public ResponseEntity<Response> getCompanies() throws ServiceException {
        return data((Serializable) compService.getCompanies());
    }

    @GetMapping("/check-cmpSubdomain/{domain}")
    public ResponseEntity<Response> checkCmpDomain(@PathVariable String domain) throws ServiceException {
        return data((Serializable) compService.checkCmpSubDomainExists(domain));
    }

    @PostMapping("/changeSubscription")
    public ResponseEntity<Response> changeSubscription(@RequestBody @Valid final Company company) throws ServiceException {
        String[] result = compService.changeSubscription(company);
        settingsService.removeInvalidNotificationSettings(company);
        return success(ResponseCode.SUBSCRIPTION_SUCCESS, result[0], result[1], result[2]);
    }

    @PostMapping("/cancelSubscription/{cmpId}")
    public ResponseEntity<Response> cancelSubscription(@PathVariable Integer cmpId) throws ServiceException {
        boolean success = compService.cancelSubscription(getCurrentUser(), cmpId);
        if (success)
            return success(ResponseCode.SUBSCRIPTION_CANCEL_SUCCESS);
        else
            return error(ResponseCode.SUBSCRIPTION_CANCEL_FAIL);
    }

    @GetMapping("/{cmpId}")
    public ResponseEntity<Response> getCompanyById(@PathVariable Integer cmpId) throws ServiceException {
        Optional<Company> optCompany = compService.getCompany(cmpId);
        if(optCompany.isPresent()){
            return data(optCompany.get());
        }

        return error(ResponseCode.UNKNOWN_COMPANY_ID, String.valueOf(cmpId));
    }

    @GetMapping("/{cmpId}/user")
    public ResponseEntity<Response> listUsers(@PathVariable final Integer cmpId) throws ServiceException {
        Boolean companyExist = compService.existsCompany(cmpId);
        if(companyExist){
            return data((Serializable) userService.getAllUserByCompanyId(cmpId));
        }

        return error(ResponseCode.UNKNOWN_COMPANY_ID, String.valueOf(cmpId));
    }

    @PostMapping("/{cmpId}/user")
    public ResponseEntity<Response> addUsers(@PathVariable final Integer cmpId, @RequestBody UserDTO userDTO) throws ServiceException {
        return data((Serializable) userService.addUser(cmpId, userDTO));
    }

    @PutMapping("/{cmpId}/user")
    public ResponseEntity<Response> editUsers(@PathVariable final Integer cmpId, @RequestBody UserDTO userDTO) throws ServiceException {
        return data((Serializable) userService.editUser(cmpId, userDTO));
    }

    @GetMapping("/{cmpId}/user/{userName}/role")
    public ResponseEntity<Response> getUserRoles(@PathVariable final Integer cmpId, @PathVariable String userName) throws ServiceException {
        Boolean companyExist = compService.existsCompany(cmpId);
        if(companyExist){
            return data((Serializable) userService.getUserRoles(userName));
        }

        return error(ResponseCode.UNKNOWN_COMPANY_ID, String.valueOf(cmpId));
    }
}
