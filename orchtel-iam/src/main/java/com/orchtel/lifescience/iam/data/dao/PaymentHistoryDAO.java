package com.orchtel.lifescience.iam.data.dao;

import com.orchtel.lifescience.iam.data.model.PaymentHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaymentHistoryDAO extends JpaRepository<PaymentHistory, Integer>{

}