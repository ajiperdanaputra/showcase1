package com.orchtel.lifescience.iam.controller;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.orchtel.lifescience.core.response.Response;
import com.orchtel.lifescience.core.response.ServiceException;
import com.orchtel.lifescience.iam.data.model.Publisher;
import com.orchtel.lifescience.iam.service.IPublisherService;

@RestController
@RequestMapping("/publisher")
public class PublisherController extends BaseController {
	
	@Autowired
    private IPublisherService publisherService;
	
	@GetMapping("/publishers")
    public ResponseEntity<Response> getPublishers() {
        return data((Serializable) publisherService.getPublishers());
    }
	
	@PostMapping("/create-publisher")
    public ResponseEntity<Response> createPublisher(@RequestBody Publisher publisher) throws ServiceException {
        return data((Serializable) publisherService.createPublisher(publisher));
    }

}
