package com.orchtel.lifescience.iam.data.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import java.util.HashSet;
import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

import com.orchtel.lifescience.iam.data.dto.SubDurationDTO;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.*;

@Data
@Entity
@Table(name = "COMPANY", uniqueConstraints={@UniqueConstraint(columnNames = {"cmpSubdomainName" , "contactEmail"})})
public class Company implements Serializable {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Integer cmpId; 
	
	@NotEmpty
	private String cmpName;
	
	private String cmpSubdomainName;
	
	@NotEmpty
	private String contactName;
	
	@NotEmpty
	private String contactEmail;
	
	@NotEmpty
	private String contactPhNumber; 
	
	@Transient
	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	private String password;
	
	private String streetAddress;
	private String city;
	private String state;
	private String country; 
	private String zipCode;
	
	//@NotEmpty
	//private String industry;
	
	@ManyToOne
    @JoinColumn(name = "INDUSTRY")
	private MasterData industry;
	
	@OneToMany(mappedBy = "company")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Set<Role> roles = new HashSet<>();
	
	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Date createTime;
	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	private Integer createUserId;
	
	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Date updateTime;
	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	private Integer updateUserId;
	
	@Transient
	private Set<Integer> items;
	
	@Transient
	private Integer subId;
	
	@Transient
	private String sessionId;
	
	@Transient
	private SubDurationDTO subDuration;
	
	@PrePersist
    public void prePersist() {
        this.createTime = new Date();
        this.updateTime = this.createTime;
    }

    @PreUpdate
    public void preUpdate() {
        this.updateTime = new Date();
    }
}