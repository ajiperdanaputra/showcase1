package com.orchtel.lifescience.iam.data.dto;

public enum SubDurationDTO {

	FREE_TRIAL,
	MONTHLY,
	YEARLY
}
