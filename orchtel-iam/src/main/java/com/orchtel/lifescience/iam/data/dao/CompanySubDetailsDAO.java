package com.orchtel.lifescience.iam.data.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.orchtel.lifescience.iam.data.model.CompanySubDetails;
import com.orchtel.lifescience.iam.data.model.CompanySubscription;

@Repository
public interface CompanySubDetailsDAO extends JpaRepository<CompanySubDetails, Integer> {

	List<CompanySubDetails> findByCompanySubscription(CompanySubscription compSub);
}
