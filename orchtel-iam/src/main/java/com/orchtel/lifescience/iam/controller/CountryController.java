package com.orchtel.lifescience.iam.controller;

import com.orchtel.lifescience.core.response.Response;
import com.orchtel.lifescience.core.response.ResponseCode;
import com.orchtel.lifescience.core.response.ServiceException;
import com.orchtel.lifescience.iam.service.ICountryOrgService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Slf4j
@RestController
@RequestMapping("/country")
public class CountryController extends BaseController {

    @Autowired
    private ICountryOrgService countryOrgService;


    @PostMapping("{code}/background-information/upload")
    public ResponseEntity<Response> uploadBackgroundInformation(@RequestParam MultipartFile file, @PathVariable String code) throws ServiceException {
        try {
            countryOrgService.uploadBackgroundInformation(code, file.getInputStream());
        } catch (IOException e) {
            log.error(e.getLocalizedMessage(), e);
        }

        return success(ResponseCode.COMPANY_ORG_FILE_UPLOADED);
    }

    @PostMapping("{code}/regulatory-authorities/upload")
    public ResponseEntity<Response> uploadRegulatoryAuthorities(@RequestParam MultipartFile file, @PathVariable String code) throws ServiceException {
        try {
            countryOrgService.uploadRegulatoryAuthorities(code, file.getInputStream());
        } catch (IOException e) {
            log.error(e.getLocalizedMessage(), e);
        }

        return success(ResponseCode.COMPANY_ORG_FILE_UPLOADED);
    }

    @PostMapping("{code}/industry-associations/upload")
    public ResponseEntity<Response> uploadIndustryAssociations(@RequestParam MultipartFile file, @PathVariable String code) throws ServiceException {
        try {
            countryOrgService.uploadIndustryAssociations(code, file.getInputStream());
        } catch (IOException e) {
            log.error(e.getLocalizedMessage(), e);
        }

        return success(ResponseCode.COMPANY_ORG_FILE_UPLOADED);
    }

    @GetMapping("/data")
    public ResponseEntity<Response> getCompanyOrgData(@RequestParam String key) throws ServiceException {
        return data(countryOrgService.getCompanyOrgContent(key));
    }

}
