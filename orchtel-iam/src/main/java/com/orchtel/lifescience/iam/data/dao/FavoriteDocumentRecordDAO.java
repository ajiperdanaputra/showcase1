package com.orchtel.lifescience.iam.data.dao;

import com.orchtel.lifescience.iam.data.model.FavoriteDocumentRecord;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface FavoriteDocumentRecordDAO extends JpaRepository<FavoriteDocumentRecord, Integer> {

    List<FavoriteDocumentRecord> findAllByUsrId(String usrId);
    Optional<FavoriteDocumentRecord> findByDocIdAndUsrId(String docId, String usrId);
}
