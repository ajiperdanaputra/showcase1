package com.orchtel.lifescience.iam.data.dao;

import com.orchtel.lifescience.iam.data.model.NotificationDocument;
import com.orchtel.lifescience.iam.data.model.NotificationSettings;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NotificationDocumentDAO extends JpaRepository<NotificationDocument, Integer>{

    @Query(nativeQuery = true,
            value = "select nd.* from NOTIFICATION_SETTINGS ns, NOTIFICATION_DOCUMENT nd where ns.NS_ID = nd.NS_ID and ACTIVE = true and nd.MD_ID = ?1 and (COUNTRY is null or COUNTRY = ?2)" )
    List<NotificationDocument> findByDocTypeIdAndCountryId(Integer docTypeId, Integer countryId);

}