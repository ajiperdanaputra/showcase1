package com.orchtel.lifescience.iam.service;

import com.orchtel.lifescience.core.response.ServiceException;
import com.orchtel.lifescience.iam.data.dto.RolesDTO;
import com.orchtel.lifescience.iam.data.model.Company;
import com.orchtel.lifescience.iam.data.model.Role;
import org.springframework.data.domain.Page;

import java.util.List;

public interface IRoleService {

    void addRole(Company company, RolesDTO role);

    void createRole(final Integer companyId, Role role, Boolean editable) throws ServiceException;

    void updateRole(final Integer companyId, final Integer roleId, Role role) throws ServiceException;

    Role getRole(final Integer companyId, final Integer roleId) throws ServiceException;

    void copyAndAssignRole(RolesDTO rolesDTO, Company company);

    Page<Role> getRoles(final Integer companyId, final String searchField, final String sortField, final String sortBy,
                        final int page, final int size) throws ServiceException;

    void deleteRole(final Integer companyId, final Integer roleId) throws ServiceException;
}
