
package com.orchtel.lifescience.iam.controller;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.orchtel.lifescience.core.response.Response;
import com.orchtel.lifescience.iam.data.dto.CountryOrgDTO;
import com.orchtel.lifescience.core.response.MasterDataTypeDTO;
import com.orchtel.lifescience.iam.data.model.MasterData;
import com.orchtel.lifescience.iam.service.ICommonService;


@RestController
@RequestMapping("/common")
public class CommonController extends BaseController {

    @Autowired
    private ICommonService commonService;

    @GetMapping("/countries")
    public ResponseEntity<Response> getCountries(@RequestParam(required = false, defaultValue = "false") Boolean isAdmin) {
        return data((Serializable) commonService.getCountriesByRegion(isAdmin));
    }

    @PostMapping("/country-code")
    public ResponseEntity<Response> getCountryCode(@RequestBody MasterData masterData) {
        return data((Serializable) commonService.getCountryByDescription(masterData.getDescription()));
    }

    @GetMapping("/organisations")
    public ResponseEntity<Response> getOrganisations() {
        return data((Serializable) commonService.getOrganisations());
    }

    @GetMapping("/industries")
    public ResponseEntity<Response> getIndustries() {
        return data((Serializable) commonService.getIndustries());
    }

    @GetMapping("/document-types")
    public ResponseEntity<Response> getDocumentTypes() {
        return data((Serializable) commonService.getDocumentTypes());
    }

    @GetMapping("/document-type-objects")
    public ResponseEntity<Response> getDocumentTypeObjects() {
        return data((Serializable) commonService.getDocumentTypeObjects());
    }

    @GetMapping("/languages")
    public ResponseEntity<Response> getLanguages() {
        return data((Serializable) commonService.getLanguages());
    } 

    @GetMapping("/modules")
    public ResponseEntity<Response> getModules() {
        return data((Serializable) commonService.getModules());
    }

    @GetMapping("/screens")
    public ResponseEntity<Response> getScreens() {
        return data((Serializable) commonService.getScreens());
    }
    
    @GetMapping("/search/organisations")
    public ResponseEntity<Response> getOrganisationsForSearch() {
        return data((Serializable) commonService.getOrganisations());
    }
    
    @PostMapping("/indices")
    public ResponseEntity<Response> getIndices(@RequestBody List<String> codes) {
        return data((Serializable) commonService.getIndices(codes));
    }
    
    @GetMapping("/documents/country-org")
    public ResponseEntity<Response> getCountryOrg() {
        return data((Serializable) commonService.getCountryOrg());
    }

    @GetMapping("/documents/country-org/type/{type}")
    public ResponseEntity<Response> getCountryOrg(@PathVariable MasterDataTypeDTO type, @RequestParam Boolean status) {
        return data((Serializable) commonService.buildCompanyOrgDtoList(commonService.getCountryOrgByTypeAndStatus(type, status)));
    }

    @GetMapping("/documents/country-org/{code}/type/{type}")
    public ResponseEntity<Response> getCountryOrgByCode(@PathVariable MasterDataTypeDTO type, @PathVariable String code) {
        return data((Serializable) commonService.getCountryOrgByTypeAndCode(type, code));
    }
    
    @PutMapping("/updateDocCount")
    public void updateDocCount(@RequestBody List<CountryOrgDTO> cogList) {
    	commonService.updateDocCount(cogList);
    }
    
    @GetMapping("/doc-details")
    public ResponseEntity<Response> getDocDetails() {
        return data((Serializable) commonService.getDocDetails());
    }
    
    @GetMapping("/country-org-list")
    public ResponseEntity<Response> getCountryOrgList() {
        return data((Serializable) commonService.getCountryOrgList());
    }
    
    @GetMapping("/getCountriesForRegion/{region}")
    public ResponseEntity<Response> getCountriesForRegion(@PathVariable String region) {
        return data((Serializable) commonService.getCountriesForRegion(region));
    }
    
    @GetMapping("/doc-country-org/{descCode}")
    public ResponseEntity<Response> getDocCountryOrg(@PathVariable String descCode) {
        return data((Serializable) commonService.getDocCountryOrg(descCode));
    }
    
    @PutMapping("/updateCountryOrg")
    public void updateCountryOrg(@RequestBody CountryOrgDTO countryOrgDto) {
    	commonService.updateCountryOrg(countryOrgDto);
    }
    
    @PostMapping("/getDocTypeId")
    public ResponseEntity<Response> getDocTypeId(@RequestBody String docType) {
        return data((Serializable) commonService.getDocTypeId(docType));
    }
}