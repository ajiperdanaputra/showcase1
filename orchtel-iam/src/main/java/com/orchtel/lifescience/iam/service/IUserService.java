package com.orchtel.lifescience.iam.service;

import com.orchtel.lifescience.core.response.ServiceException;
import com.orchtel.lifescience.iam.data.dto.*;
import com.orchtel.lifescience.iam.data.model.Company;
import com.orchtel.lifescience.iam.data.model.UserRole;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public interface IUserService {

	UserDTO getUserByEmail(String email) throws ServiceException;
	
	UserDTO getUser(String username);

	UserDTO getUserById(String usrId);

	List<UserDTO> listAllUsers(boolean activeOnly) throws ServiceException;

	UserDTO getUserByEmailAndCompanyId(String email, Integer cmpId) throws ServiceException;

	UserDTO createUser(Company company, RolesDTO assignedRole) throws ServiceException;
	
	String login(LoginRequestDTO loginRequestDTO) throws ServiceException;
	
	String verifyEmail(String username) throws ServiceException;
	
	boolean verifyPhone(String username, Integer otp, boolean isSignup) throws ServiceException;

	UserDTO sendOTP(String username) throws ServiceException;
	
	void forgotPassword(String email) throws ServiceException;
	
	void resetPassword(PasswordRecoveryDTO passwordRecoveryDTO) throws ServiceException;
	
	void changePassword(ChangePasswordDTO changePasswordDTO) throws ServiceException;
	
	ProfileDTO getUserProfile(UserDTO userDTO) throws ServiceException;
	
	void updateUserProfile(UserDTO currentUserDTO, UserDTO userDTO) throws ServiceException;

	ArrayList<String> checkMFA(String usrId) throws ServiceException;

	void disableUser(String username) throws ServiceException;

	void uploadPhoto(String usrId, InputStream is, String fileName) throws ServiceException;

	StreamingResponseBody getPhoto(String usrId, PhotoTypeDTO photoTypeDTO) throws ServiceException;

	void removePhoto(String usrId) throws ServiceException;

	ArrayList<ProfileDTO> listNetworkExpertUsers(String usrId, HashMap<String, String> searchNetworkExpertParamMap) throws ServiceException;

	UserDTO getNetworkExpertUser(String username, String requestorUsrId);

	void toggleFavoriteNetworkExpert(String usrId, String netExpUsrId) throws ServiceException;

	List<UserDTO> getAllUserByCompanyId(final Integer companyId) throws ServiceException;

    UserDTO addUser(Integer cmpId, UserDTO userDTO) throws ServiceException;

	UserDTO editUser(Integer cmpId, UserDTO userDTO) throws ServiceException;

    List<UserRole> getUserRoles(String userName);

    void remindInactiveUser() throws ServiceException;

	ArrayList<UserDTO> getNetworkExpertUsersByCountry(String country) throws ServiceException;

	ArrayList<String> getFavoriteDocumentRecord(String ursId);

	void toggleFavoriteDocumentRecord(String docId, String usrId) throws ServiceException;
}