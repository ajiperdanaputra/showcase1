package com.orchtel.lifescience.iam.service;

import com.orchtel.lifescience.core.response.ServiceException;
import com.orchtel.lifescience.iam.data.dto.PhotoTypeDTO;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import java.io.IOException;
import java.io.InputStream;

public interface IAwsS3Service {

    void uploadPhoto(String usrId, InputStream is, String fileName) throws ServiceException;
    StreamingResponseBody getPhoto(String usrId, PhotoTypeDTO photoTypeDTO) throws ServiceException;
    void removePhoto(String usrId) throws ServiceException;

    void uploadCountryOrgData(InputStream is, String fileName) throws ServiceException;

    String getCompanyOrgContent(String key) throws IOException;
}
