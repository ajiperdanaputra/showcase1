package com.orchtel.lifescience.iam.data.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.orchtel.lifescience.core.response.MasterDataTypeDTO;
import com.orchtel.lifescience.iam.data.model.MasterData;

@Repository
public interface MasterDataDAO extends JpaRepository<MasterData, Integer> {

	List<MasterData> findByType(MasterDataTypeDTO type);
	List<MasterData> findByTypeAndDescriptionContains(MasterDataTypeDTO type, String keyword);
	Optional<MasterData> findByTypeAndCode(MasterDataTypeDTO type, String code);
	MasterData findByTypeAndDescription(MasterDataTypeDTO type, String description);
	MasterData findByTypeAndDescriptionIgnoreCase(MasterDataTypeDTO type, String description);
	List<MasterData> findByParent(MasterData masterData);
	
	@Query("SELECT m.description from MasterData AS m WHERE m.type = :type order by m.description")
	List<String> findAllByType(@Param("type") MasterDataTypeDTO type);

	List<MasterData> findAllByTypeOrderByDescription(MasterDataTypeDTO type);
}
