package com.orchtel.lifescience.iam.data.dao;

import com.orchtel.lifescience.iam.data.dto.SubDurationDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.orchtel.lifescience.iam.data.dto.SubStatusDTO;
import com.orchtel.lifescience.iam.data.model.Company;
import com.orchtel.lifescience.iam.data.model.CompanySubscription;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface CompanySubscriptionDAO extends JpaRepository<CompanySubscription, Integer> {

	CompanySubscription findByCompanyAndStatus(Company company, SubStatusDTO status);
	List<CompanySubscription> findByCompanyCmpId(Integer cmpId);
	List<CompanySubscription> findAllBySubscribePeriodAndStatusAndExpiryDateBetween(SubDurationDTO duration, SubStatusDTO status, Date date1, Date date2);
	Optional<CompanySubscription> findBySubPaymentDetailsTransactionId(String transactionId);
	List<CompanySubscription> findAllByStatusAndExpiryDateBetween(SubStatusDTO status, Date date1, Date date2);
}
