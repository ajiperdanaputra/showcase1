package com.orchtel.lifescience.iam.data.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.orchtel.lifescience.core.response.MasterDataTypeDTO;
import lombok.Data;

@Data
@Entity

@Table(name = "MASTER_DATA")
public class MasterData implements Comparable<MasterData>, Serializable {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer mdId;

	@ManyToOne
    @JoinColumn(name = "PARENT_ID")
    @OneToMany
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	private MasterData parent;

	@Enumerated(EnumType.STRING)
	private MasterDataTypeDTO type;
	
	private String code;
	private String description;
	
	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	private Integer order;

	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Date createTime;
	
	@Transient
	private Integer parentId;
	
	@Override
    public int compareTo(MasterData data) {
        return getDescription().compareToIgnoreCase(data.getDescription());
    }
}
