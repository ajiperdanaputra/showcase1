package com.orchtel.lifescience.iam.controller;

import com.orchtel.lifescience.iam.data.dto.UserDTO;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.orchtel.lifescience.core.controller.Controller;
import com.orchtel.lifescience.core.filter.RequestContext;
import com.orchtel.lifescience.core.response.ServiceException;
import com.orchtel.lifescience.iam.service.IUserService;

public class BaseController extends Controller {

    @Autowired
    protected IUserService userService;

    protected Authentication authentication() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null)
            MDC.put(RequestContext.REQUEST_USER_MDC, authentication.getName());

        return authentication;
    }

    protected UserDTO getCurrentUser() throws ServiceException {
        return userService.getUser(authentication().getName());
    }
}