package com.orchtel.lifescience.iam.service;

import com.orchtel.lifescience.iam.data.dto.NotificationSettingDTO;
import com.orchtel.lifescience.iam.data.model.Company;
import com.orchtel.lifescience.iam.data.model.NotificationSettings;
import com.orchtel.lifescience.iam.data.model.UserMfa;

import java.util.List;

public interface ISettingsService {

    String addNotification(NotificationSettingDTO notificationSettings);
    List<NotificationSettings> getNotifications(String usrId);
    boolean toggleNotification(Integer id, String usrId);
    boolean removeNotification(Integer id, String usrId);
    boolean updateNotification(NotificationSettingDTO notificationSettings);
    boolean toggleMfa(String usrId, List<UserMfa> mfaSettings);
    void removeInvalidNotificationSettings(Company company);
}
