package com.orchtel.lifescience.iam.data.dao;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.orchtel.lifescience.iam.data.dto.SubPlanNameDTO;
import com.orchtel.lifescience.iam.data.dto.SubTypeDTO;
import com.orchtel.lifescience.iam.data.model.Subscription;

@Repository
public interface SubscriptionDAO extends JpaRepository<Subscription, Integer> {

	Optional<Subscription> findBySubPlanNameAndSubType(SubPlanNameDTO subPlanNameDTO, SubTypeDTO subTypeDTO);
	ArrayList<Subscription> findAllByEnabled(Boolean enabled);
}
