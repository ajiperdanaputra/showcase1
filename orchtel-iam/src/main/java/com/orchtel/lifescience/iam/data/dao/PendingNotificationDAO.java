package com.orchtel.lifescience.iam.data.dao;

import com.orchtel.lifescience.iam.data.dto.OccurrenceDTO;
import com.orchtel.lifescience.iam.data.model.PendingNotification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface PendingNotificationDAO extends JpaRepository<PendingNotification, Integer> {

    @Query(nativeQuery = true,
            value = "select * from PENDING_NOTIFICATION where USR_ID = ?1 and (IN_APP_ACK = false or ACTIVITY_TIMESTAMP >= ?2) order by ACTIVITY_TIMESTAMP desc, IN_APP_ACK asc" )
    List<PendingNotification> getInAppNotificationPreview(String usrId, Date timestamp);

    List<PendingNotification> findAllByUsrIdOrderByInAppAckAscActivityTimestampDesc(String usrId);
    void deleteByPnoIdIn(List<Integer> ids);
    List<PendingNotification> findAllByNotifiedAndChannelsNotNullAndChannelsNotAndOccurrenceAndActivityTimestampBetween(
            Boolean notified, String emptyString, OccurrenceDTO occurrenceDTO, Date d1, Date d2
    );
}
