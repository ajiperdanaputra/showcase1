package com.orchtel.lifescience.iam.controller;

import com.orchtel.lifescience.core.response.Response;
import com.orchtel.lifescience.core.response.ResponseCode;
import com.orchtel.lifescience.core.response.ServiceException;
import com.orchtel.lifescience.iam.data.model.Role;
import com.orchtel.lifescience.iam.service.IRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.io.Serializable;

/**
 * @author vikas on 21/12/20
 */
@RestController
@RequestMapping("company/{companyId}/role")
public class RoleController extends BaseController {

    @Autowired
    private IRoleService roleService;

    @PostMapping
    public ResponseEntity<Response> createRole(@PathVariable Integer companyId, @Valid @RequestBody Role role) throws ServiceException {
        logger.info("Request to create role {} for the company {}", role, companyId);
        roleService.createRole(companyId, role, Boolean.TRUE);
        logger.info("Role successfully created");
        return success(ResponseCode.ROLE_CREATED, role.getRoleName());
    }

    @PutMapping("/{roleId}")
    public ResponseEntity<Response> updateRole(@PathVariable Integer companyId,
                                               @PathVariable Integer roleId,
                                               @Valid @RequestBody Role role) throws ServiceException {
        logger.info("Request to update role {} for the company {}", role, companyId);
        roleService.updateRole(companyId, roleId, role);
        logger.info("Role successfully updated");
        return success(ResponseCode.ROLE_UPDATED, role.getRoleName());
    }

    @DeleteMapping("/{roleId}")
    public ResponseEntity<Response> deleteRole(@PathVariable Integer companyId, @PathVariable Integer roleId) throws ServiceException {
        logger.info("Request to delete role {} for the company {}", roleId, companyId);
        roleService.deleteRole(companyId, roleId);
        logger.info("Role successully deleted");
        return success(ResponseCode.ROLE_DELETED);
    }

    @GetMapping("/{roleId}")
    public ResponseEntity<Response> getRole(@PathVariable Integer companyId, @PathVariable Integer roleId) throws ServiceException {
        return data((Serializable) roleService.getRole(companyId, roleId));
    }

    @GetMapping
    public ResponseEntity<Response> getRoles(@PathVariable Integer companyId,
                                             @RequestParam(required = false) String searchField,
                                             @RequestParam(required = false) String sortField,
                                             @RequestParam(required = false) String sortBy,
                                             @RequestParam(defaultValue = "0") int page,
                                             @RequestParam(defaultValue = "10") int size) throws ServiceException {
        return data((Serializable) roleService.getRoles(companyId, searchField, sortField, sortBy, page, size));
    }
}
