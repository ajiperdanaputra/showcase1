package com.orchtel.lifescience.iam.service;

import java.util.List;

import com.amazonaws.services.cognitoidp.model.AttributeType;
import com.amazonaws.services.cognitoidp.model.UserType;
import com.orchtel.lifescience.core.response.ServiceException;
import com.orchtel.lifescience.iam.data.dto.ChangePasswordDTO;
import com.orchtel.lifescience.iam.data.dto.PasswordRecoveryDTO;
import com.orchtel.lifescience.iam.data.dto.UserDTO;
import com.orchtel.lifescience.iam.data.dto.UserPaginationDto;


public interface IAwsCognitoService {

    UserDTO signup(UserDTO awsCognitoUserDTODTO) throws ServiceException;
    
    String login(String username, String password) throws ServiceException;

    UserDTO getUser(String accessToken) throws ServiceException;
    
    UserDTO getUserByUsername(String username) throws ServiceException;

    UserDTO getUserBySub(String sub) throws ServiceException;

    UserDTO getUserByEmail(String email) throws ServiceException;

	void resetPassword(PasswordRecoveryDTO passwordRecoveryDTO) throws ServiceException;
	
	void changePassword(ChangePasswordDTO changePasswordDTO) throws ServiceException;

	void deleteUser(String developerAccountId) throws ServiceException;

    void disableUser(String developerAccountId) throws ServiceException;
	
	void updateUserAttribute(String username, List<AttributeType> attributeTypes) throws ServiceException;
	
	void confirmUser(String username) throws ServiceException;

	List<UserType> listUsers() throws ServiceException;

    UserPaginationDto listUsers(final Integer companyId, final String paginationToken, final Integer limit) throws ServiceException;

    void createGroup(final Integer companyId, final String companyName) throws ServiceException;

    void addUserToGroup(final Integer companyId, final String userId) throws ServiceException;

    UserDTO getUserByEmailAndCompanyId(String email, Integer cmpId) throws ServiceException;
} 