package com.orchtel.lifescience.iam.service;

import java.util.List;
import java.util.Set;

import com.orchtel.lifescience.core.response.ServiceException;
import com.orchtel.lifescience.iam.data.dto.SubPlanNameDTO;
import com.orchtel.lifescience.iam.data.dto.SubStatusDTO;
import com.orchtel.lifescience.iam.data.dto.SubTypeDTO;
import com.orchtel.lifescience.iam.data.model.Company;
import com.orchtel.lifescience.iam.data.model.CompanySubDetails;
import com.orchtel.lifescience.iam.data.model.CompanySubscription;
import com.orchtel.lifescience.iam.data.model.Subscription;

public interface ISubscriptionService {

	List<Subscription> getSubscriptions();
	Subscription getSubscription(Integer subId);
	Subscription getSubscription(SubPlanNameDTO subName, SubTypeDTO subTypeDTO);
	Set<CompanySubDetails> validateSubscriptionItems(CompanySubscription compSubscription, Set<Integer> items) throws ServiceException;
	void addCompanySubscription(CompanySubscription cmpSub);
	CompanySubscription getCompanySubscription(Company company, SubStatusDTO status);
	List<CompanySubDetails> getSubscriptionItems(CompanySubscription cmpSub);
	void trialExpiryReminder();
	void trialExpiryNotification();
	void setUnpaidSubscriptionExpired();
	CompanySubscription getCompanySubscriptionByTransactionId(String transactionId);
	CompanySubscription extendSubscription(String transactionId);
}