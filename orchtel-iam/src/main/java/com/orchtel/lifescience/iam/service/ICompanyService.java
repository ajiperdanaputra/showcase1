package com.orchtel.lifescience.iam.service;

import java.util.List;
import java.util.Optional;

import com.orchtel.lifescience.core.response.ServiceException;
import com.orchtel.lifescience.iam.data.dto.UserDTO;
import com.orchtel.lifescience.iam.data.model.Company;

public interface ICompanyService {

	void addCompany(Company company) throws ServiceException;
	Optional<Company> getCompany(Integer compId) throws ServiceException;
	Optional<Company> getCompany(String compName) throws ServiceException;
	List<Company> getCompanies();
	boolean checkCmpSubDomainExists(String domain) throws ServiceException;
	boolean checkUserWithDomainExists(String email, String domain) throws ServiceException;
	boolean cancelSubscription(UserDTO userDTO, Integer cmpId) throws ServiceException;
	boolean expireSubscription(UserDTO userDTO, Integer cmpId) throws ServiceException;
	String[] changeSubscription(Company company) throws ServiceException;
	void updateCompanyProfile(Company company) throws ServiceException;
	Boolean existsCompany(Integer companyId) throws ServiceException;
}