package com.orchtel.lifescience.iam.config;

import java.text.SimpleDateFormat;

import org.springframework.boot.autoconfigure.security.oauth2.resource.ResourceServerProperties;
import org.springframework.context.annotation.*;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;
import org.springframework.web.filter.AbstractRequestLoggingFilter;
import org.springframework.web.filter.CommonsRequestLoggingFilter;
import org.springframework.web.filter.RequestContextFilter;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.orchtel.lifescience.core.config.AWSConfig;
import com.orchtel.lifescience.core.config.DataConfig;
import com.orchtel.lifescience.core.config.LocaleConfig;
import com.orchtel.lifescience.core.config.SecurityConfig;
import com.orchtel.lifescience.core.filter.AppControllerAdvice;
import com.orchtel.lifescience.core.filter.CORSFilter;
import com.orchtel.lifescience.core.filter.SubDomainFilter;

@Configuration
@ComponentScan(basePackages = { "com.orchtel.lifescience.core.service.impl" })
@Import({AppControllerAdvice.class, AWSConfig.class, DataConfig.class, LocaleConfig.class})
@PropertySources({
	@PropertySource("classpath:application-core.properties"),
	@PropertySource(value = "classpath:application-core-${spring.profiles.active}.properties", ignoreResourceNotFound = true)
})
public class AppConfig extends SecurityConfig {
	
    public AppConfig(ResourceServerProperties resource) {
		super(resource);
	}

	@Bean
    public ObjectMapper objectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        objectMapper.setDateFormat(df);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.enable(MapperFeature.ACCEPT_CASE_INSENSITIVE_ENUMS);

        return objectMapper;
    }

    @Bean
    public AbstractRequestLoggingFilter requestLoggingFilter() {
        AbstractRequestLoggingFilter loggingFilter = new CommonsRequestLoggingFilter();
        loggingFilter.setIncludeClientInfo(true);
        loggingFilter.setIncludeQueryString(true);
        loggingFilter.setIncludePayload(true);
        loggingFilter.setMaxPayloadLength(2048);
        return loggingFilter;
    }
    
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
    
	@Override
	public void configure(HttpSecurity http) throws Exception {

		http.csrf().disable();
		http.addFilterBefore(new CORSFilter(), ChannelProcessingFilter.class)
			.addFilterBefore(new RequestContextFilter(), CORSFilter.class) 
			.addFilterBefore(new SubDomainFilter(), RequestContextFilter.class)
			.authorizeRequests()
			.antMatchers(
					"/v2/api-docs",
                    "/configuration/ui",
                    "/swagger-resources/**",
                    "/configuration/security",
                    "/swagger-ui.html",
                    "/webjars/**",
                    "/payments/stripe/webhook/*",
					"/company/signup",
					"/user/verify-email/**",
					"/user/send-otp/**",
					"/user/verify-phone/**",
					"/user/forgot-password/**",
					"/user/reset-password",
					"/payments/stripe-create-session/**",
					"/common/**",
					"/company/check-cmpSubdomain/**",
					"/user/check-email/**",
					"/user/login", "/user/check-mfa/**",
					"/publisher/publishers"
					)
			.permitAll()
			.antMatchers(HttpMethod.GET, "/payments/success/**").permitAll()
				.antMatchers(HttpMethod.GET, "/payments/failed/**").permitAll()
			.antMatchers(HttpMethod.GET, "/subscriptions/**")
			.permitAll()
			.antMatchers("/actuator/health")
			.permitAll()
			.anyRequest().authenticated();
	}
}