package com.orchtel.lifescience.iam.data.dao;

import com.orchtel.lifescience.core.response.ServiceException;
import com.orchtel.lifescience.iam.data.model.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.orchtel.lifescience.iam.data.model.UserRole;

import java.util.List;

@Repository
public interface UserRoleDAO extends JpaRepository<UserRole, Integer>{

	List<UserRole> findByUsrId(String usrId);
	List<UserRole> findByRoleRoleNameAndCompanyCmpSubdomainName(String role, String cmpSubdomainName);
	boolean existsByUsrIdAndRoleRoleDescriptionContains(String usrId, String keyword);
	List<UserRole> findByCompanyCmpId(Integer cmpId);
	List<UserRole> findByCompanyIn(List<Company> companies);

	@Query("select count(ur) from com.orchtel.lifescience.iam.data.model.UserRole ur where ur.company.cmpId = ?1")
	Long totalUserByCompanyId(final Integer cmpId);

	@Query("select count(ur) from com.orchtel.lifescience.iam.data.model.UserRole ur where ur.company.cmpId = ?1 and ur.role.rolId = ?2")
	Integer totalUserByCompanyIdAndRoleId(final Integer cmpId, final Integer roleId);

	List<UserRole> findAllByUsrId(final String usrId);

	void deleteAllByUsrIdAndCompanyCmpId(final String usrId, final Integer cmpId) throws ServiceException;

	List<UserRole> findAllByUsrIdIn(List<String> usrIds);

	Boolean existsByUsrId(String usrId);

	UserRole findByUsrIdAndRoleRoleName(final String usrId, final String role);
}