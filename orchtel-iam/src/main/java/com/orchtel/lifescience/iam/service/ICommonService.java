package com.orchtel.lifescience.iam.service;

import java.util.List;
import java.util.Map;
import java.util.SortedSet;

import com.orchtel.lifescience.iam.data.dto.CountryOrgDTO;
import com.orchtel.lifescience.iam.data.dto.DocDetailsDTO;
import com.orchtel.lifescience.core.response.MasterDataTypeDTO;
import com.orchtel.lifescience.iam.data.model.CountryOrg;
import com.orchtel.lifescience.iam.data.model.MasterData;
import com.orchtel.lifescience.iam.data.model.Modules;
import com.orchtel.lifescience.iam.data.model.Screen;

public interface ICommonService {

	Map<String, SortedSet<CountryOrg>> getCountriesByRegion(Boolean isAdmin);

	CountryOrg getCountryByDescription(String description);
	
	List<MasterData> getIndustries();
	
	List<String> getDocumentTypes();

    List<MasterData> getDocumentTypeObjects();
	
	List<MasterData> getLanguages();

    List<Modules> getModules();

    List<Screen> getScreens();

    List<CountryOrg> getOrganisations();
    
    List<String> getIndices(List<String> codes);
    
    List<CountryOrgDTO> getCountryOrg();

    List<CountryOrg> getCountryOrgByTypeAndStatus(MasterDataTypeDTO typeDTO, Boolean status);

    CountryOrgDTO getCountryOrgByTypeAndCode(MasterDataTypeDTO typeDTO, String code);

    void updateDocCount(List<CountryOrgDTO> cogList);
    
    DocDetailsDTO getDocDetails();
    
    List<String> getCountryOrgList();
    
    List<String> getCountriesForRegion(String region);
    
    CountryOrgDTO getDocCountryOrg(String descCode);
    
    void updateCountryOrg(CountryOrgDTO countryOrgDto);
    
    Integer getDocTypeId(String docType);
    
    List<CountryOrgDTO> buildCompanyOrgDtoList(List<CountryOrg> countryOrgList);
  
}
