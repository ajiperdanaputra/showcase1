package com.orchtel.lifescience.iam.controller;

import com.orchtel.lifescience.core.response.Response;
import com.orchtel.lifescience.core.response.ResponseCode;
import com.orchtel.lifescience.core.response.ServiceException;
import com.orchtel.lifescience.iam.data.dto.NotificationSettingDTO;
import com.orchtel.lifescience.iam.data.model.UserMfa;
import com.orchtel.lifescience.iam.service.ISettingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.Serializable;
import java.util.List;

@RestController
@RequestMapping("/settings")
public class SettingsController extends BaseController{

    @Autowired
    private ISettingsService settingsService;

    @PostMapping("/add-notification")
    public ResponseEntity<Response> addNotificationSettings(@RequestBody @Valid final NotificationSettingDTO notificationSettings) {
        String usrId = settingsService.addNotification(notificationSettings);
        if(usrId != null)
            return success(ResponseCode.SETTINGS_NOTIFICATION_ADDED, usrId);
        else
            return error(ResponseCode.SETTINGS_INVALID_NOTIFICATION_DATA);
    }

    @GetMapping("/notifications/{usrId}")
    public ResponseEntity<Response> getNotificationSettings(@PathVariable String usrId) {
        return data((Serializable) settingsService.getNotifications(usrId));
    }

    @PostMapping("/toggle-notification/{id}/{usrId}")
    public ResponseEntity<Response> toggleNotificationSettings(@PathVariable String id, @PathVariable String usrId) {
        return data((Serializable) settingsService.toggleNotification(Integer.parseInt(id), usrId));
    }

    @PostMapping("/remove-notification/{id}/{usrId}")
    public ResponseEntity<Response> removeNotificationSettings(@PathVariable String id, @PathVariable String usrId) {
        boolean success = settingsService.removeNotification(Integer.parseInt(id), usrId);
        if(success)
            return success(ResponseCode.SETTINGS_NOTIFICATION_DELETED, id);
        else
            return error(ResponseCode.SETTINGS_NOTIFICATION_NOT_EXIST, id);
    }

    @PostMapping("/update-notification")
    public ResponseEntity<Response> updateNotificationSettings(@RequestBody @Valid final NotificationSettingDTO notificationSettingDTO) {
        boolean success = settingsService.updateNotification(notificationSettingDTO);
        if(success)
            return success(ResponseCode.SETTINGS_NOTIFICATION_UPDATED);
        else
            return error(ResponseCode.SETTINGS_NOTIFICATION_NOT_EXIST,
                    notificationSettingDTO.getNsId()!=null?notificationSettingDTO.getNsId().toString():null);
    }

    @PostMapping("/toggle-mfa/{usrId}")
    public ResponseEntity<Response> toggleMfa(@PathVariable String usrId, @RequestBody List<UserMfa> userMfaList) throws ServiceException {
        boolean result = settingsService.toggleMfa(usrId, userMfaList);
        return data((Serializable) result);
    }
}
