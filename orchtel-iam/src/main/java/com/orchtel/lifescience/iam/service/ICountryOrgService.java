package com.orchtel.lifescience.iam.service;

import com.orchtel.lifescience.core.response.ServiceException;

import java.io.InputStream;

public interface ICountryOrgService {

    void uploadBackgroundInformation(String code, InputStream inputStream) throws ServiceException;

    void uploadRegulatoryAuthorities(String code, InputStream inputStream) throws ServiceException;

    void uploadIndustryAssociations(String code, InputStream inputStream) throws ServiceException;

    String getCompanyOrgContent(String key) throws ServiceException;
}