package com.orchtel.lifescience.iam.data.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.amazonaws.services.cognitoidp.model.AttributeType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO implements Serializable {
	
	private static final Logger logger = LoggerFactory.getLogger(UserDTO.class);

	private String userStatus;
	private Boolean enabled;
	private String sub;
	private String username;
	private Integer cmpId;
	private String email;
	private String password;
	private String name;
	private String phone;
	private String address;
	private String city;
	private String state;
	private String country; 
	private String zipCode;
    private Boolean emailVerified;
    private Boolean phoneVerified;
    private String aboutMe;
    private String aboutMeExt1;
    private List<String> skills;
    private List<String> languages;
    private List<String> associatedCountries;
    private String linkedinURL;
    private String qualification;

    private Long registrationTime;
    private Boolean favorite;

    private List<String> roles;

    private String designation;
    private Boolean allowDelete;


    public static UserDTO convertFromAttributeTypeList(List<AttributeType> list) {
        UserDTO userDTO = com.orchtel.lifescience.iam.data.dto.UserDTO.builder().favorite(false).build();
        for (AttributeType attributeType : list) {
            switch (attributeType.getName()) {
            	case "sub":
            		userDTO.setSub(attributeType.getValue());
            		break;
            	case "email":
                    userDTO.setEmail(attributeType.getValue());
                    break;
                case "name":
                    userDTO.setName(attributeType.getValue());
                    break;
                case "phone_number":
                    userDTO.setPhone(attributeType.getValue());
                    break;
                case "custom:address":
                    userDTO.setAddress(attributeType.getValue());
                    break;
                /*case "locale":
                    userDTO.setLocale(attributeType.getValue());
                    break; */
                case "email_verified":    
                    userDTO.setEmailVerified(Boolean.valueOf(attributeType.getValue()));
                    break;
                case "phone_number_verified":    
                    userDTO.setPhoneVerified(Boolean.valueOf(attributeType.getValue()));
                    break;    
                case "custom:cmpId":
            		userDTO.setCmpId(Integer.valueOf(attributeType.getValue()));
            		break;
                case "custom:city":
                    userDTO.setCity(attributeType.getValue());
                    break;
                case "custom:state":
                    userDTO.setState(attributeType.getValue());
                    break;
                case "custom:country":
                    userDTO.setCountry(attributeType.getValue());
                    break;
                case "custom:zipCode":
                    userDTO.setZipCode(attributeType.getValue());
                    break;
                case "custom:aboutMe":
                    userDTO.setAboutMe(attributeType.getValue());
                    break;
                case "custom:aboutMeExt1":
                    userDTO.setAboutMeExt1(attributeType.getValue());
                    break;
                case "custom:skills":
                    userDTO.setSkills(attributeType.getValue()==null||attributeType.getValue().equals("-") ?
                            null : Arrays.asList(attributeType.getValue().split(",")));
                    break;
                case "custom:languages":
                    userDTO.setLanguages(attributeType.getValue()==null||attributeType.getValue().equals("-") ?
                            null : Arrays.asList(attributeType.getValue().split(",")));
                    break;
                case "custom:associatedCountries":
                    userDTO.setAssociatedCountries(attributeType.getValue()==null||attributeType.getValue().equals("-") ?
                            new ArrayList<>() : Arrays.asList(attributeType.getValue().split(",")));
                    break;
                case "custom:linkedinURL":
                    userDTO.setLinkedinURL(attributeType.getValue());
                    break;
                case "custom:qualification":
                    userDTO.setQualification(attributeType.getValue());
                    break;
                case "custom:designation":
                    userDTO.setDesignation(attributeType.getValue());
                    break;
                default:
                	logger.error("Invalid Mapping : {}", attributeType.getName());
                    break;
            }
        }
        
        return userDTO;
    }
    
    private static void addAttribute(List<AttributeType> userAttributes, String name, String value) {
    	AttributeType attribute = new AttributeType();
    	attribute.setName(name);
    	attribute.setValue(value);
        userAttributes.add(attribute);
    }

    public static List<AttributeType> convertToAttributeTypeList(UserDTO userDTO) {
        List<AttributeType> userAttributes = new ArrayList<AttributeType>();

        if (!StringUtils.isEmpty(userDTO.getEmail()))
        	addAttribute(userAttributes, "email", userDTO.getEmail());

        if (!StringUtils.isEmpty(userDTO.getName()))
        	addAttribute(userAttributes, "name", userDTO.getName());

        if (!StringUtils.isEmpty(userDTO.getPhone()))
        	addAttribute(userAttributes, "phone_number", userDTO.getPhone());

        if (!StringUtils.isEmpty(userDTO.getAddress()))
        	addAttribute(userAttributes, "custom:address", userDTO.getAddress());
        	
        //if (!StringUtils.isEmpty(userDTO.getLocale()))
        //	addAttribute(userAttributes, "locale", userDTO.getLocale());

        if (!StringUtils.isEmpty(userDTO.getCmpId()))
        	addAttribute(userAttributes, "custom:cmpId", userDTO.getCmpId().toString());
        
        if (!StringUtils.isEmpty(userDTO.getCity()))
        	addAttribute(userAttributes, "custom:city", userDTO.getCity());
        
        if (!StringUtils.isEmpty(userDTO.getState()))
        	addAttribute(userAttributes, "custom:state", userDTO.getState());
        
        if (!StringUtils.isEmpty(userDTO.getCountry()))
        	addAttribute(userAttributes, "custom:country", userDTO.getCountry());
        
        if (!StringUtils.isEmpty(userDTO.getZipCode()))
        	addAttribute(userAttributes, "custom:zipCode", userDTO.getZipCode());
        
        if (!StringUtils.isEmpty(userDTO.getAboutMe()))
            addAttribute(userAttributes, "custom:aboutMe", userDTO.getAboutMe());

        if (!StringUtils.isEmpty(userDTO.getAboutMeExt1()))
            addAttribute(userAttributes, "custom:aboutMeExt1", userDTO.getAboutMeExt1());

        if (userDTO.getSkills()!=null && userDTO.getSkills().isEmpty())
            userDTO.setSkills(null);
        addAttribute(userAttributes, "custom:skills",
                userDTO.getSkills()==null ? "-" : String.join(",", userDTO.getSkills()));

        if (userDTO.getLanguages()!=null && userDTO.getLanguages().isEmpty())
            userDTO.setLanguages(null);
        addAttribute(userAttributes, "custom:languages",
                userDTO.getLanguages()==null ? "-" : String.join(",", userDTO.getLanguages()));

        if (userDTO.getAssociatedCountries()!=null && userDTO.getAssociatedCountries().isEmpty())
            userDTO.setAssociatedCountries(new ArrayList<>());
        addAttribute(userAttributes, "custom:associatedCountries",
                userDTO.getAssociatedCountries()==null ? "-" : String.join(",", userDTO.getAssociatedCountries()));

        if (!StringUtils.isEmpty(userDTO.getLinkedinURL()))
            addAttribute(userAttributes, "custom:linkedinURL", userDTO.getLinkedinURL());

        if (!StringUtils.isEmpty(userDTO.getQualification()))
            addAttribute(userAttributes, "custom:qualification", userDTO.getQualification());

        if (!StringUtils.isEmpty(userDTO.getDesignation()))
            addAttribute(userAttributes, "custom:designation", userDTO.getDesignation());

        return userAttributes;
    }
}