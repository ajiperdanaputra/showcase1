package com.orchtel.lifescience.iam.controller;

import java.io.Serializable;
import java.util.Optional;

import com.orchtel.lifescience.core.response.ResponseCode;
import com.orchtel.lifescience.core.response.ServiceException;
import com.orchtel.lifescience.iam.data.dto.SubStatusDTO;
import com.orchtel.lifescience.iam.data.model.Company;
import com.orchtel.lifescience.iam.service.ICompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.orchtel.lifescience.core.response.Response;
import com.orchtel.lifescience.iam.data.dto.SubPlanNameDTO;
import com.orchtel.lifescience.iam.data.dto.SubTypeDTO;
import com.orchtel.lifescience.iam.service.ISubscriptionService;

@RestController
@RequestMapping("/subscriptions")
public class SubscriptionController extends BaseController {

	@Autowired
	private ISubscriptionService subscriptionService;

	@Autowired
	private ICompanyService companyService;
	
	@GetMapping
	public ResponseEntity<Response> getSubscriptions() {
		return data((Serializable) subscriptionService.getSubscriptions());
	}
	
	@GetMapping("/{subPlanNameDTO}/{subTypeDTO}")
	public ResponseEntity<Response> getSubscription(@PathVariable SubPlanNameDTO subPlanNameDTO, @PathVariable SubTypeDTO subTypeDTO) {
		return data((Serializable) subscriptionService.getSubscription(subPlanNameDTO, subTypeDTO));
	}
	
	@GetMapping("/{subId}")
	public ResponseEntity<Response> getSubscription(@PathVariable Integer subId) {
		return data((Serializable) subscriptionService.getSubscription(subId));
	}

	@GetMapping("/company/{cmpId}")
	public ResponseEntity<Response> getCompanySubscription(@PathVariable Integer cmpId) throws ServiceException {
		Optional<Company> optCompany = companyService.getCompany(cmpId);
		if(optCompany.isPresent()){
			Company company = optCompany.get();
			return data((Serializable) subscriptionService.getCompanySubscription(company, SubStatusDTO.ACTIVE));
		}

		return error(ResponseCode.UNKNOWN_COMPANY_ID, String.valueOf(cmpId));
	}
}
