package com.orchtel.lifescience.iam.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import com.orchtel.lifescience.iam.data.dto.*;
import com.orchtel.lifescience.iam.service.ICompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.orchtel.lifescience.core.response.Response;
import com.orchtel.lifescience.core.response.ResponseCode;
import com.orchtel.lifescience.core.response.ServiceException;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

@RestController
@RequestMapping("/user")
public class UserController extends BaseController {

	@Autowired
	private ICompanyService compService;

	@PostMapping("/verify-email/{username}")
	public ResponseEntity<Response> verifyUserEmail(@PathVariable String username) throws ServiceException {
		String email = userService.verifyEmail(username);
        return success(ResponseCode.EMAIL_VERIFIED, email);
	}
	
	@PostMapping("/send-otp/{username}")
	public ResponseEntity<Response> sendOTP(@PathVariable String username) throws ServiceException {
		return data(userService.sendOTP(username));
	}
	
	@PostMapping("/verify-phone/{username}/{otp}")
	public ResponseEntity<Response> verifyPhone(@PathVariable String username, @PathVariable Integer otp, @RequestBody(required = false) String signup) throws ServiceException {
		return data(userService.verifyPhone(username, otp, Boolean.parseBoolean(signup)));
	}
	
	@PostMapping("/forgot-password/{email}")
	public ResponseEntity<Response> forgotPassword(@PathVariable String email) throws ServiceException {
		userService.forgotPassword(email);
		return success(ResponseCode.FORGOT_PASSWORD_SENT, email);
	}
	
	@PostMapping("/reset-password")
	public ResponseEntity<Response> resetPassword(@RequestBody PasswordRecoveryDTO passwordRecoveryDTO) throws ServiceException {
		userService.resetPassword(passwordRecoveryDTO);
		return success(ResponseCode.PASSWORD_RESET, passwordRecoveryDTO.getUsername());
	}
	
	@PostMapping("/change-password")
	public ResponseEntity<Response> changePassword(@RequestHeader(value="authorization") String accessToken, @RequestBody ChangePasswordDTO changePasswordDTO) throws ServiceException {
		changePasswordDTO.setAccessToken(accessToken.split(" ")[1]);
		userService.changePassword(changePasswordDTO);
		return success(ResponseCode.PASSWORD_CHANGE);
	}
	
	@GetMapping("/profile/{username}/{requestorUsrId}")
	public ResponseEntity<Response> getUser(@PathVariable String username, @PathVariable String requestorUsrId) throws ServiceException {
		return data((Serializable) userService.getNetworkExpertUser(username, requestorUsrId));
	}

	@GetMapping("/profile/{username}")
	public ResponseEntity<Response> getUser(@PathVariable String username) throws ServiceException {
		return data((Serializable) userService.getUser(username));
	}
	
	@GetMapping("/full-profile")
	public ResponseEntity<Response> getProfile() throws ServiceException {
		return data((Serializable) userService.getUserProfile(getCurrentUser()));
	}
	
	@PostMapping("/profile")
	public ResponseEntity<Response> updateUser(@RequestBody UserDTO userDTO) throws ServiceException {
		userService.updateUserProfile(getCurrentUser(), userDTO);
		return success(ResponseCode.USER_PROFILE_UPDATED);
	}
	
	@GetMapping("/check-email/{email}")
	public ResponseEntity<Response> checkEmail(@PathVariable String email) throws ServiceException {
		UserDTO userDTO = userService.getUserByEmail(email);
		if (userDTO != null) {
			return data(true);
		} else {
			return data(false);
		}
	}

	@GetMapping("/check-email")
	public @ResponseBody String checkEmailByParam(@RequestParam(required = false, defaultValue = "app") String domain,
													  @RequestParam String email) throws ServiceException {
		return Boolean.toString(!compService.checkUserWithDomainExists(email, domain));
	}

	@GetMapping("/check-email/{email}/{subdomain}")
	public ResponseEntity<Response> checkCmpDomain(@PathVariable String email, @PathVariable String subdomain) throws ServiceException {
		return data((Serializable) compService.checkUserWithDomainExists(email, subdomain));
	}
	
	@PostMapping("/login")
	public ResponseEntity<Response> updateUser(@RequestBody @Valid LoginRequestDTO loginRequestDTO) throws ServiceException {
		return data(userService.login(loginRequestDTO));
	}

	@PostMapping("/check-mfa/{usrId}")
	public ResponseEntity<Response> checkMFA(@PathVariable String usrId) throws ServiceException {
		ArrayList<String> mfaTypes = userService.checkMFA(usrId);
		if(mfaTypes != null && mfaTypes.size() > 0){
			return data(mfaTypes);
		}
		return data(new ArrayList<>());
	}

	@PostMapping("/disable/{username}")
	public ResponseEntity<Response> disableUser(@PathVariable String username) throws ServiceException {
		userService.disableUser(username);
		return success(ResponseCode.USER_DISABLED, username);
	}

	@PostMapping("/uploadPhoto/{usrId}")
	public ResponseEntity<Response> uploadPhoto(@RequestParam MultipartFile userImage, @PathVariable String usrId) throws ServiceException {
		try {
			userService.uploadPhoto(usrId, userImage.getInputStream(), userImage.getOriginalFilename());
		} catch (IOException e) {
			e.printStackTrace();
		}

		return success(ResponseCode.USER_PHOTO_UPDATED);
	}

	@GetMapping("/photo/{photoType}/{usrId}")
	public ResponseEntity<StreamingResponseBody> downloadFile(@PathVariable PhotoTypeDTO photoType,
															  @PathVariable String usrId) {
		try {
			ResponseEntity<StreamingResponseBody> result =
					new ResponseEntity<StreamingResponseBody>(userService.getPhoto(usrId, photoType), HttpStatus.OK);
			if(result != null) {
				return result;
			}

		} catch (ServiceException e) {
			e.printStackTrace();
			return null;
		}

		throw new ResponseStatusException(HttpStatus.NOT_FOUND, "entity not found" );
	}

	@PostMapping("/remove-photo/{usrId}")
	public ResponseEntity<Response> removePhoto(@PathVariable String usrId) throws ServiceException {
		userService.removePhoto(usrId);
		return success(ResponseCode.USER_PHOTO_UPDATED);
	}

	@GetMapping("/network-expert-users/{usrId}")
	public ResponseEntity<Response> listNetworkExpertUsers(@PathVariable String usrId) throws ServiceException {
		return data(userService.listNetworkExpertUsers(usrId, null));
	}

	@GetMapping("/network-expert-users-by-country")
	public ResponseEntity<Response> listNetworkExpertUsersByCountry(@RequestParam String country) throws ServiceException {
		return data(userService.getNetworkExpertUsersByCountry(country));
	}

	@PostMapping("/search-network-expert-users/{usrId}")
	public ResponseEntity<Response> searchNetworkExpertUsers(@PathVariable String usrId, @RequestBody HashMap<String, String> searchNetworkExpertParamMap) throws ServiceException {
		return data(userService.listNetworkExpertUsers(usrId, searchNetworkExpertParamMap));
	}

	@PostMapping("/toggle-favorite-network-expert/{usrId}/{networkExpertId}")
	public ResponseEntity<Response> toggleFavoriteNetworkExpert(@PathVariable String usrId, @PathVariable String networkExpertId) throws ServiceException {
		userService.toggleFavoriteNetworkExpert(usrId, networkExpertId);
		return success(ResponseCode.FAVORITE_NETWORK_EXPERT_TOGGLED);
	}

	@PostMapping("/get-favorite-document/{usrId}")
	public ResponseEntity<Response> anyFavoriteDocument(@PathVariable String usrId) throws ServiceException {
		return data(userService.getFavoriteDocumentRecord(usrId));
	}

	@PostMapping("/toggle-favorite-document-record/{usrId}/{docId}")
	public ResponseEntity<Response> toggleFavoriteDocumentRecord(@PathVariable String usrId, @PathVariable String docId) throws ServiceException {
		userService.toggleFavoriteDocumentRecord(docId, usrId);
		return success(ResponseCode.OK);
	}
}
