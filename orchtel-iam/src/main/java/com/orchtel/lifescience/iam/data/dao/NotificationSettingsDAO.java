package com.orchtel.lifescience.iam.data.dao;

import com.orchtel.lifescience.iam.data.model.NotificationSettings;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface NotificationSettingsDAO extends JpaRepository<NotificationSettings, Integer>{

    List<NotificationSettings> findByUsrId(String usrId);
    List<NotificationSettings> findByUsrIdIn(List<String> userIds);
}