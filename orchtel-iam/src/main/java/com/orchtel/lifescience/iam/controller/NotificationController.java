package com.orchtel.lifescience.iam.controller;

import com.orchtel.lifescience.core.response.Response;
import com.orchtel.lifescience.core.response.ResponseCode;
import com.orchtel.lifescience.core.response.ServiceException;
import com.orchtel.lifescience.iam.data.dto.*;
import com.orchtel.lifescience.iam.data.model.Subscription;
import com.orchtel.lifescience.iam.service.INotificationService;
import com.orchtel.lifescience.iam.service.ISubscriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;

/**
 * @author vikas on 31/12/20
 */
@RestController
@RequestMapping("/notification")
public class NotificationController extends BaseController{

    @Autowired
    private INotificationService notificationService;

    @Autowired
    private ISubscriptionService subscriptionService;

    @PostMapping("/send-sms")
    public ResponseEntity<Response> sendSms(@RequestBody @Valid final SmsNotificationDTO notificationDTO) throws ServiceException {
        notificationService.sendSms(notificationDTO.getMessage(), notificationDTO.getPhone());
        return success(ResponseCode.SMS_NOTIFICATION_SENT);
    }

    @PostMapping("/change-password")
    public ResponseEntity<Response> changePassword(@RequestBody UserDTO userDTO) throws ServiceException {
        notificationService.notifyPasswordChange(userDTO);
        return success(ResponseCode.EMAIL_CHANGE_PASSWORD_SENT);
    }

    @PostMapping("/payment-received")
    public ResponseEntity<Response> paymentReceived(@RequestBody ProfileDTO profileDTO) throws ServiceException {
        Subscription subscription = subscriptionService.getSubscription(profileDTO.getCompany().getSubId());
        notificationService.paymentReceived(profileDTO.getUser(), subscription, profileDTO.getCompany().getSubDuration());
        return success(ResponseCode.SMS_NOTIFICATION_SENT);
    }

    @PostMapping("/publish-notification")
    public ResponseEntity<Response> publishNotification(@RequestBody @Valid PublishNotificationDTO publishNotificationDTO){
        try {
            notificationService.publishNotification(publishNotificationDTO);
            return success(ResponseCode.DOCUMENT_NOTIFICATION_SUBMITTED);
        } catch (ServiceException e) {
            e.printStackTrace();
            return error(ResponseCode.DOCUMENT_NOTIFICATION_ERROR, e.getMessage());
        }
    }

    @PostMapping("/in-app-notification/{viewMode}/{usrId}")
    public ResponseEntity<Response> retrieveInAppNotificationPreview(@PathVariable InAppNotificationViewModeDTO viewMode, @PathVariable String usrId){
        return data(notificationService.getInAppNotifications(usrId, viewMode.equals(InAppNotificationViewModeDTO.PREVIEW)));
    }

    @PostMapping("/delete-in-app-notification")
    public ResponseEntity<Response> deleteInAppNotification(@RequestBody ArrayList<Integer> notifIdList){
        notificationService.deleteInAppNotifications(notifIdList);
        return success(ResponseCode.IN_APP_NOTIFICATION_DELETED);
    }

    @PostMapping("/ack-in-app-notification")
    public ResponseEntity<Response> ackInAppNotification(@RequestBody ArrayList<Integer> notifIdList){
        return data(notificationService.ackInAppNotifications(notifIdList));
    }
}
