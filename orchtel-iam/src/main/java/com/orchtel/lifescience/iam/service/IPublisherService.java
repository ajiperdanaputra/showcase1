package com.orchtel.lifescience.iam.service;

import java.util.List;

import com.orchtel.lifescience.core.response.ServiceException;
import com.orchtel.lifescience.iam.data.model.Publisher;

public interface IPublisherService {
	
	List<String> getPublishers();
	
	List<String> createPublisher(Publisher publisher) throws ServiceException;

}
