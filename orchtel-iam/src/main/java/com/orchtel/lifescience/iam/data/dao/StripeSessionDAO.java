package com.orchtel.lifescience.iam.data.dao;

import com.orchtel.lifescience.iam.data.model.StripeSession;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StripeSessionDAO extends JpaRepository<StripeSession, Integer> {

    Optional<StripeSession> findByAppSession(String appSession);

}