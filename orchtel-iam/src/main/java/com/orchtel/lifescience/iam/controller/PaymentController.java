package com.orchtel.lifescience.iam.controller;

import com.orchtel.lifescience.core.response.Response;
import com.orchtel.lifescience.core.response.ResponseCode;
import com.orchtel.lifescience.core.response.ServiceException;
import com.orchtel.lifescience.iam.data.dto.StripeSessionTypeDTO;
import com.orchtel.lifescience.iam.data.model.Company;
import com.orchtel.lifescience.iam.service.impl.StripeServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.Serializable;

@RestController
@RequestMapping("/payments")
public class PaymentController extends BaseController {

    @Autowired
    private StripeServiceImpl stripeService;

    @PostMapping("stripe-create-session/{sessionType}/{tempSessionId}")
    public ResponseEntity<Response> createSession(@RequestBody Company company, @PathVariable StripeSessionTypeDTO sessionType, @PathVariable String tempSessionId) throws ServiceException {
        return data((Serializable) stripeService.createSession(company, sessionType, tempSessionId));
    }

    @GetMapping(value = "/success", produces = MediaType.TEXT_HTML_VALUE)
    public ResponseEntity<String> successPayment(@RequestParam final String tempSession) throws ServiceException {
        return ResponseEntity.ok().contentType(MediaType.TEXT_HTML).body(stripeService.paymentSuccess(tempSession));
    }

    @GetMapping(value = "/failed", produces = MediaType.TEXT_HTML_VALUE)
    public ResponseEntity<String> failedPayment(@RequestParam final String tempSession) throws ServiceException {
        return ResponseEntity.ok().contentType(MediaType.TEXT_HTML).body(stripeService.paymentFailed(tempSession));
    }

    @PostMapping("/stripe/webhook/invoice-payment-succeeded")
    public ResponseEntity<Response> invoicePaymentSucceeded(@RequestBody String payload) {
        try {
            stripeService.webhookInvoicePayment(true, payload);
        } catch(Throwable t){
            logger.error("stripe/webhook (invoicePaymentSucceeded) error: " + t.getMessage() + ", payload: " + payload);
        }
        return success(ResponseCode.OK);
    }

    @PostMapping("/stripe/webhook/invoice-payment-failed")
    public ResponseEntity<Response> invoicePaymentFailed(@RequestBody String payload) {
        try{
            stripeService.webhookInvoicePayment(false, payload);
        } catch(Throwable t){
            logger.error("stripe/webhook (invoicePaymentFailed) error: " + t.getMessage() + ", payload: " + payload);
        }
        return success(ResponseCode.OK);
    }

    @PostMapping("/stripe/webhook/invoice-upcoming")
    public ResponseEntity<Response> invoiceUpcoming(@RequestBody String payload) {
        try{
            stripeService.webhookInvoiceUpcoming(payload);
        } catch(Throwable t){
            logger.error("stripe/webhook (invoicePaymentFailed) error: " + t.getMessage());
        }
        return success(ResponseCode.OK);
    }
}
