package com.orchtel.lifescience.iam.data.dao;

import com.orchtel.lifescience.iam.data.model.Company;
import com.orchtel.lifescience.iam.data.model.Role;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RoleDAO extends JpaRepository<Role, Integer>, JpaSpecificationExecutor<Role> {

    Optional<Role> findByRoleName(String roleName);

    Optional<Role> findByRoleNameAndCompanyCmpId(String roleName, Integer cmpId);
    Optional<Role> findByRoleNameAndCompanyIsNull(String roleName);

    Boolean existsByRolIdNotAndRoleNameIgnoreCaseAndCompany(final Integer roleId, final String name, final Company company);
    Boolean existsByRoleNameIgnoreCaseAndCompany(final String name, final Company company);
    Boolean existsByRolIdAndCompany(final Integer roleId, final Company company);

    List<Role> findAllByCompany(final Company company);
    Page<Role> findAllByCompany(final Company company, Pageable pageable);
    Page<Role> findAllByCompanyAndRoleNameContainingIgnoreCase(final Company company, String roleName, Pageable pageable);
}
