package com.orchtel.lifescience.iam.data.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.FetchType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "ROLES")
public class Role implements Serializable {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer rolId;
	
	@ManyToOne
    @JoinColumn(name = "CMP_ID")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Company company;
	
	private String roleName;
	private String roleDescription;

    @Column(name = "IS_EDITABLE")
    private Boolean editable;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Date createTime;
	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	private Integer createUserId;

	@Transient
	private Integer noOfUser;
	
	@OneToMany(mappedBy = "role", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @Builder.Default
    private Set<RolePrivileges> rolePrivileges = new HashSet<>();

    @PrePersist
    public void prePersist() {
        this.createTime = new Date();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Role role = (Role) o;
        return rolId.equals(role.rolId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(rolId);
    }
}
