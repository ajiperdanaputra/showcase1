package com.orchtel.lifescience.iam.data.dao;

import com.orchtel.lifescience.iam.data.model.FavoriteNetworkExpert;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;


public interface FavoriteNetworkExpertDAO extends JpaRepository<FavoriteNetworkExpert, Integer> {

    List<FavoriteNetworkExpert> findAllByUsrId(String usrId);
    Optional<FavoriteNetworkExpert> findByUsrIdAndNetworkExpertUsrIdAndActive(String usrId, String expertNetworkUsrId, Boolean active);
}
