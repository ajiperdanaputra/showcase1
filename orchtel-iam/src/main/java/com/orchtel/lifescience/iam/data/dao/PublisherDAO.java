package com.orchtel.lifescience.iam.data.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.orchtel.lifescience.iam.data.model.Publisher;

public interface PublisherDAO extends JpaRepository<Publisher, Integer> {
	
	@Query("SELECT p.publisherName from Publisher AS p order by p.publisherName")
	List<String> findAllPublishers();
	
	boolean existsByPublisherCode(String publisherCode);
	boolean existsByPublisherName(String publisherName);

}
