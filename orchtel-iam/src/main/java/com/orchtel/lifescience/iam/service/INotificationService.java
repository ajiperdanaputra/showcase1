package com.orchtel.lifescience.iam.service;

import com.orchtel.lifescience.core.response.ServiceException;
import com.orchtel.lifescience.iam.data.dto.*;
import com.orchtel.lifescience.iam.data.model.Subscription;

import java.util.ArrayList;
import java.util.List;

public interface INotificationService {

	void sendVerificationEmail(UserDTO userDTO);
	void sendForgotPasswordEmail(UserDTO userDTO, int code);

    void sendSms(final String message, final String phone);

    void sendOTP(String phone, int otp);
    void sendOTPByEmail(UserDTO userDTO, int otp);

    void notifyPasswordChange(UserDTO userDTO);
    void accountActivation(UserDTO userDTO);
    void paymentReceived(UserDTO userDTO, Subscription subscription, SubDurationDTO subDurationDTO);
    void paymentFailed(UserDTO userDTO, Subscription subscription, SubDurationDTO subDurationDTO);
    void upcomingSubscriptionRenewal(UserDTO userDTO, Subscription subscription, SubDurationDTO subDurationDTO, long daysBetween);
    void subscriptionExpiredNotification(UserDTO userDTO);
    void changeSubscription(UserDTO userDTO, Subscription subscription, SubDurationDTO subDurationDTO);
    void unsubscribe(UserDTO userDTO, Subscription subscription, SubDurationDTO subDurationDTO);
    void disableUser(UserDTO userDTO);
    void publishNotification(PublishNotificationDTO publishNotificationDTO) throws ServiceException;
    void broadcastPendingNotification(OccurrenceDTO occurrenceDTO);
    void remindInactiveUsers(UserDTO userDTO);
    ArrayList<InAppNotificationPreviewDTO> getInAppNotifications(String usrId, boolean preview);
    void deleteInAppNotifications(List<Integer> ids);
    String ackInAppNotifications(List<Integer> ids);
    void trialExpiryReminder(UserDTO userDTO, int daysBeforeExpired, String timestampStr);
    void trialExpiryReminderSMS(UserDTO userDTO, int daysBeforeExpired, String timestampStr);
    void trialExpiryNotification(UserDTO userDTO);
    void trialExpiryNotificationSMS(UserDTO userDTO);
}
