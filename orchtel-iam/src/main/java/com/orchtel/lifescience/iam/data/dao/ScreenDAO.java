package com.orchtel.lifescience.iam.data.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.orchtel.lifescience.iam.data.model.Screen;

import java.util.List;

@Repository
public interface ScreenDAO extends JpaRepository<Screen, Integer> {

    public List<Screen> findAllByModId(final Integer modId);

    public List<Screen> findAllByModIdIsNullOrderByScrNameAsc();

}
