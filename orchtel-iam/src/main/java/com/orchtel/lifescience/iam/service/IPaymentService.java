package com.orchtel.lifescience.iam.service;

import java.math.BigDecimal;
import java.math.RoundingMode;

import com.orchtel.lifescience.core.response.ServiceException;
import com.orchtel.lifescience.iam.data.dto.StripeSessionTypeDTO;
import com.orchtel.lifescience.iam.data.model.Company;
import com.orchtel.lifescience.iam.data.model.CompanySubscription;
import com.orchtel.lifescience.iam.data.model.SubPaymentDetails;
import com.stripe.model.checkout.Session;

public interface IPaymentService {

	String createSession(Company company, StripeSessionTypeDTO sessionType, String tempSessionId) throws ServiceException;
	Session getSession(String sessionId) throws ServiceException;
	SubPaymentDetails confirmPayment(Session session) throws ServiceException;
	boolean cancelSubscription(CompanySubscription companySubscription) throws ServiceException;
	
	default Long toCents(BigDecimal amount) {
		return amount.setScale(2, RoundingMode.HALF_DOWN)
				.multiply(BigDecimal.valueOf(100L))
				.longValue();
	}
	
	default BigDecimal fromCents(Long amount) {
		return new BigDecimal(amount).divide(BigDecimal.valueOf(100L)).setScale(2, RoundingMode.HALF_DOWN);
	}

	void webhookInvoicePayment(boolean paymentSuccess, String payload);
	void webhookInvoiceUpcoming(String payload);

    String paymentSuccess(String tempSessionId) throws ServiceException;

	String paymentFailed(String tempSessionId) throws ServiceException;
}