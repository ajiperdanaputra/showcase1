package com.orchtel.lifescience.iam.data.dao;

import com.orchtel.lifescience.iam.data.model.Modules;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ModulesDAO extends JpaRepository<Modules, Integer> {

}
