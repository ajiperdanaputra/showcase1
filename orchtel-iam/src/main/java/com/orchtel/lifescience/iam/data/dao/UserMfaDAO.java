package com.orchtel.lifescience.iam.data.dao;

import com.orchtel.lifescience.iam.data.model.UserMfa;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserMfaDAO extends JpaRepository<UserMfa, Integer> {

    List<UserMfa> findByUsrIdAndActive(String usrId, Boolean status);
    List<UserMfa> findByUsrId(String usrId);
}
