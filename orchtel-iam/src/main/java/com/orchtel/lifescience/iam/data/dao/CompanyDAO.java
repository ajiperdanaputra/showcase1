package com.orchtel.lifescience.iam.data.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.orchtel.lifescience.iam.data.model.Company;

@Repository
public interface CompanyDAO extends JpaRepository<Company, Integer> {

	Optional<Company> findByCmpNameIgnoreCase(String orgName);
	
	boolean existsByCmpSubdomainName(String domain);

	boolean existsByContactEmailIgnoreCaseAndCmpSubdomainNameIgnoreCase(String email, String domain);

	Optional<Company> findByCmpSubdomainName(String domain);

	Optional<Company> findByContactEmailIgnoreCaseAndCmpSubdomainName(String email, String subDomain);
}